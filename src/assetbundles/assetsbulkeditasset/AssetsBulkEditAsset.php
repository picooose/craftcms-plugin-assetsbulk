<?php
/**
 * AssetsBulkEdit plugin for Craft CMS 3.x
 *
 * Edit assets in bulk
 *
 * @link      www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\assetsbulkedit\assetbundles\assetsbulkeditasset;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Adrien Picard
 * @package   AssetsBulkEdit
 * @since     1
 */
class AssetsBulkEditAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@apstudio/assetsbulkedit/assetbundles/assetsbulkeditasset/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/AssetsBulkEdit.js',
        ];

        $this->css = [
            'css/AssetsBulkEdit.css',
        ];

        parent::init();
    }
}
