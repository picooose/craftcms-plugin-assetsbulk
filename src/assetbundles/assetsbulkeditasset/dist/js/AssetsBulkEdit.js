if (typeof Craft.AssetsBulkEdit === typeof undefined) {
  Craft.AssetsBulkEdit = {};
}

(function($) {
  Craft.AssetsBulkEdit.Edit = Craft.BaseElementEditor.extend({
    init: function(type){
      var self = this;

      var resizeTrigger = new Craft.ElementActionTrigger({
        type: type,
        batch: true,
        validateSelection: function($selectedItems) {
          var settings, element = $selectedItems.first().find('.element').first();

          // Param mapping
          if (typeof settings === 'undefined' && $.isPlainObject(element)) {
            settings = element;
            element = null;
          }

          self.$element = $(element);
          self.setSettings(settings, Craft.BaseElementEditor.defaults);

          var elements = $selectedItems.find('[data-type*=Asset]').length;
          return (elements > 1) ? true : false;
        },
        activate: function($selectedItems) {
          let assetIds = $selectedItems.find('[data-type*=Asset]').map(function(i, asset){ return $(asset).data('id') });
          self.loadHud(assetIds.toArray());
        }
      });
    },
    loadHud: function(assetIds) {
      this.onBeginLoading();
      var data          = this.getBaseData();
      data.includeSites = this.settings.showSiteSwitcher;
      data.assetIds     = assetIds;
      Craft.postActionRequest('assets-bulk-edit/default/get-form', data, $.proxy(this, 'showHud'));
    },
    saveElement: function() {
      var validators = this.settings.validators;

      if ($.isArray(validators)) {
        for (var i = 0; i < validators.length; i++) {
          if ($.isFunction(validators[i]) && !validators[i].call()) {
            return false;
          }
        }
      }

      this.$spinner.removeClass('hidden');
      var data     = $.param(this.getBaseData()) + '&' + this.hud.$body.serialize();

      Craft.postActionRequest('assets-bulk-edit/default/save-element', data, $.proxy(function(response, textStatus) {
        this.$spinner.addClass('hidden');

        if (textStatus === 'success') {
          if (response.success) {
            this.closeHud();
            Craft.elementIndex.updateElements()
          }
          else {
            this.updateForm(response);
            Garnish.shake(this.hud.$hud);
          }
        }
      }, this));
    }
  });

})(jQuery);
