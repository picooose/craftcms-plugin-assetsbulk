<?php
/**
 * AssetsBulkEdit plugin for Craft CMS 3.x
 *
 * Edit assets in bulk
 *
 * @link      www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\assetsbulkedit\controllers;

use apstudio\assetsbulkedit\AssetsBulkEdit;
use apstudio\assetsbulkedit\elements\AssetBulkModel;

use Craft;
use craft\base\Element;
use craft\base\ElementInterface;
use craft\errors\InvalidTypeException;
use craft\controllers\ElementsController;
use craft\helpers\ArrayHelper;
use craft\helpers\ElementHelper;
use craft\helpers\StringHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
/**
 * @author    Adrien Picard
 * @package   AssetsBulkEdit
 * @since     1
 */
class DefaultController extends ElementsController
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['get-form', 'save-element'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionGetForm(){
        $request   = Craft::$app->getRequest();
        $elementId = $request->getBodyParam('elementId');
        $assetIds  = $request->getBodyParam('assetIds');
        $element   = AssetBulkModel::findOne($elementId);

        return $this->_getEditorHtmlResponse($element, $assetIds);
    }

    /**
     * Saves an element.
     *
     * @return Response
     * @throws NotFoundHttpException if the requested element cannot be found
     * @throws ForbiddenHttpException if the user is not permitted to edit the requested element
     */
    public function actionSaveElement(): Response{
        /** @var Element $element */
        $request   = Craft::$app->getRequest();
        $elementIds = $request->getBodyParam('elementIds');

        $success = true;

        foreach ($elementIds as $elementId) {
            $element   = AssetBulkModel::findOne($elementId);

            // Figure out where the data will be in POST
            $namespace = Craft::$app->getRequest()->getRequiredBodyParam('namespace');

            // Configure the element
            $params = Craft::$app->getRequest()->getBodyParam($namespace, []);
            ArrayHelper::remove($params, 'fields');
            Craft::configure($element, $params);

            // Set the custom field values
            $element->setFieldValuesFromRequest($namespace . '.fields');

            // Now save it
            if ($element->enabled && $element->enabledForSite) {
                $element->setScenario(Element::SCENARIO_LIVE);
            }

            $success = Craft::$app->getElements()->saveElement($element);
        }

        if($success){
            $response = [
                'success' => true,
                'id' => $element->id,
                'siteId' => $element->siteId,
                'newTitle' => (string)$element,
                'cpEditUrl' => $element->getCpEditUrl(),
            ];

            // Should we be including table attributes too?
            $sourceKey = Craft::$app->getRequest()->getBodyParam('includeTableAttributesForSource');

            if ($sourceKey) {
                $attributes = Craft::$app->getElementIndexes()->getTableAttributes(get_class($element), $sourceKey);

                // Drop the first one
                array_shift($attributes);

                foreach ($attributes as $attribute) {
                    $response['tableAttributes'][$attribute[0]] = $element->getTableAttributeHtml($attribute[0]);
                }
            }

            return $this->asJson($response);
        }


        return $this->_getEditorHtmlResponse($element, $elementIds);
    }

    /**
     * Returns the editor HTML response for a given element.
     *
     * @param ElementInterface $element
     * @param bool $includeSites
     * @return Response
     * @throws ForbiddenHttpException if the user is not permitted to edit content in any of the sites supported by this element
     */
    private function _getEditorHtmlResponse(ElementInterface $element, $assetIds): Response {
        /** @var Element $element */
        $siteIds = ElementHelper::editableSiteIdsForElement($element);

        if (empty($siteIds)) {
            throw new ForbiddenHttpException('User not permitted to edit content in any of the sites supported by this element');
        }

        $response = [];
        $response['siteId'] = $element->siteId;

        $namespace = 'editor_' . StringHelper::randomString(10);
        $this->getView()->setNamespace($namespace);

        $response['html'] = '<input type="hidden" name="namespace" value="' . $namespace . '">';


        foreach ($assetIds as $id) { //asset ids
            $response['html'] .= '<input type="hidden" name="elementIds[]" value="' . $id . '">';
        }

        if ($element->siteId !== null) {
            $response['html'] .= '<input type="hidden" name="siteId" value="' . $element->siteId . '">';
        }

        $response['html'] .= '<div class="meta">' .
            $this->getView()->namespaceInputs((string)$element->getEditorHtml()) .
            '</div>';

        $view = $this->getView();
        $response['headHtml'] = $view->getHeadHtml();
        $response['footHtml'] = $view->getBodyHtml();

        return $this->asJson($response);
    }

}
