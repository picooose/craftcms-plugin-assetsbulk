<?php
namespace apstudio\assetsbulkedit\elementactions;

use Craft;
use craft\base\ElementAction;
use craft\helpers\Json;

use apstudio\assetsbulkedit\assetbundles\assetsbulkeditasset\AssetsBulkEditAsset;

class BulkEdit extends ElementAction
{
    /**
     * @return string
     */
    public function getTriggerLabel(): string
    {
        return Craft::t('assets-bulk-edit', 'Bulk edit');
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function getTriggerHtml()
    {
        $type = Json::encode(static::className());

        Craft::$app->view->registerAssetBundle(AssetsBulkEditAsset::class);
        Craft::$app->view->registerJs('new Craft.AssetsBulkEdit.Edit('. $type .');');
    }
}