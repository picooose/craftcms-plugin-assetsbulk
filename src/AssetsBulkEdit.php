<?php
/**
 * AssetsBulkEdit plugin for Craft CMS 3.x
 *
 * Edit assets in bulk
 *
 * @link      www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\assetsbulkedit;

use apstudio\assetsbulkedit\elementactions\BulkEdit;

use apstudio\assetsbulkedit\services\AssetsBulkEditService as AssetsBulkEditServiceService;

use Craft;
use craft\base\Plugin;
use craft\elements\Asset;
use craft\events\AssetEvent;
use craft\events\RegisterElementActionsEvent;
use craft\events\RegisterUserPermissionsEvent;
use craft\services\UserPermissions;

use yii\base\Event;

/**
 * Class AssetsBulkEdit
 *
 * @author    Adrien Picard
 * @package   AssetsBulkEdit
 * @since     1
 *
 * @property  AssetsBulkEditServiceService $assetsBulkEditService
 */
class AssetsBulkEdit extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var AssetsBulkEdit
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Register asset actions
        Event::on(Asset::class, Asset::EVENT_REGISTER_ACTIONS, [$this, 'registerAssetActions']);

        // Register user permissions
        Event::on(UserPermissions::class, UserPermissions::EVENT_REGISTER_PERMISSIONS, [$this, 'registerUserPermissions']);
    }

    /**
     * @param RegisterElementActionsEvent $event
     */
    public function registerAssetActions(RegisterElementActionsEvent $event)
    {
        if (Craft::$app->getUser()->checkPermission('assetsBulkEdit-bulkEdit')) {
            $event->actions[] = new BulkEdit();
        }
    }

    /**
     * @param RegisterUserPermissionsEvent $event
     */
    public function registerUserPermissions(RegisterUserPermissionsEvent $event)
    {
        $event->permissions[Craft::t('assets-bulk-edit', 'Assets Bulk Edit')] = [
            'assetsBulkEdit-bulkEdit' => ['label' => Craft::t('bulk-edit', 'Bulk Edit')],
        ];
    }

    // Protected Methods
    // =========================================================================

}
