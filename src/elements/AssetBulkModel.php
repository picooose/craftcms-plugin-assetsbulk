<?php

namespace apstudio\assetsbulkedit\elements;

use apstudio\assetsbulkedit\AssetsBulkEdit;

use Craft;
use craft\base\Element;
use craft\elements\Asset;
use craft\elements\db\ElementQuery;
use craft\elements\db\ElementQueryInterface;

/**
 * @author    fds
 * @package   Sdfds
 * @since     1
 */
class AssetBulkModel extends Asset{
    /**
     * @inheritdoc
     */
    public function getEditorHtml(): string
    {
        if (!$this->fieldLayoutId) {
            $this->fieldLayoutId = Craft::$app->getRequest()->getBodyParam('defaultFieldLayoutId');
        }

        $html = Element::getEditorHtml();

        return $html;
    }
}