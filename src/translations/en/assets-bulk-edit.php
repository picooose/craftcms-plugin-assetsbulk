<?php
/**
 * AssetsBulkEdit plugin for Craft CMS 3.x
 *
 * Edit assets in bulk
 *
 * @link      www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

/**
 * @author    Adrien Picard
 * @package   AssetsBulkEdit
 * @since     1
 */
return [
    'AssetsBulkEdit plugin loaded' => 'AssetsBulkEdit plugin loaded',
];
